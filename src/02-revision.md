## Revision { data-transition="fade none" }

###### Data types

* Data types are created with the `data` keyword
* Data types have zero or more constructors
* Data type constructors each have zero or more *arguments*
* Data types are case-analysed by *pattern-matching*

---

###### Type classes

* Type classes are created with the `class` keyword
* Type classes specify the *type* of member functions
* Type class *instances* specify implementation for a data type (`instance` keyword)
* Function type signatures containing a `=>` always appear left-most; a *type class constraint*
* There is always at most one instance of a data type for a given type class

