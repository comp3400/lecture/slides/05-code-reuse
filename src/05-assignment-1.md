## Assignment 1 { data-transition="fade none" }

###### Assignment 1 has been published on Blackboard

----

## Assignment 1 { data-transition="fade none" }

###### Let's play a game

* There is a pool of numbers from one to nine
* There are two players
* Each player will alternately select a number from the pool
* Once a number is selected, it can never be selected again
* The first player to have *exactly three* of *any* of their selected numbers add to 15, wins the game
* If the pool of numbers is exhausted, the game ends in a draw

----

## Assignment 1 { data-transition="fade none" }

###### Some consequences of the game rules

* The game has a maximum of nine moves *(or technically correct: nine plies)*
  * If the game ends in a draw, then player 1 had five moves and player 2 had four moves
* No player can win before move 3
* A player may have selected 4 numbers, but exactly 3 of those add to 15 -> they win

----

## Assignment 1 { data-transition="fade none" }

###### Blackboard

* Think about this game, how you might model it with a program
* The full assignment with details is on Blackboard
* If you have *any problems at all*, technical or otherwise, it is *very important* that you contact Tony or Brian
  * by email privately
  * by IRC online chat (linked from Blackboard)

