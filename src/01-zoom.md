## Online Lectures { data-transition="fade none" }

###### This lecture is recorded

* Be advised that this lecture is recorded
* The recording will be uploaded to Blackboard after the lecture
* Please mute your microphone at the start of the lecture

---

###### Interacting

* Please feel free to ask questions during the lecture
* You can do this by:
  * from the members window, select "Raise Hand"
  * I will stop at some point and ask you for your question
  * take your microphone off mute
  * ask your question
  * when you have finished talking, please put your microphone back on mute
* If there are any questions that require significant time to answer, or require some further interaction, I will ask to defer the question, and reschedule another Zoom session in the future

---

