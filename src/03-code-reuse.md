## Code Reuse { data-transition="fade none" }

###### Avoiding repetition

* We are already familiar with software engineering principles such as avoiding code repetition
* Let's look at a familiar example

---

###### Suppose the following scenario

* We are an automobile manufacturer
* We have an inventory of automobiles (`inventory :: [Automobile]`)
* And we wish to ask the question, "is this particular `Automobile` in the inventory?"

---

###### Suppose the following scenario

* We might first consider a function with the following type:

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- is the given automobile in the inventory?
inventoryContains :: Automobile -> [Automobile] -> Bool
</code></pre>

---

###### Suppose the following scenario

* However, we are smarter than that, and exercise our software engineering principles
* That way, we can reuse this function when we wish to ask the same question about other inventories

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- is the given &lt;anything&gt; in the inventory?
inventoryContains :: a -> [a] -> Bool
</code></pre>

---

###### Suppose the following scenario

* So we implement it accordingly

<pre><code class="language-haskell hljs" data-trim data-noescape>
inventoryContains :: a -> [a] -> Bool
inventoryContains _ [] =
  False
inventoryContains x (h:t) =
  (x == h) || inventoryContains x t
</code></pre>

---

###### Suppose the following scenario

* But we receive a type error

<pre><code class="language-haskell hljs" data-trim data-noescape>
inventoryContains :: a -> [a] -> Bool
inventoryContains _ [] =
  False
inventoryContains x (h:t) =
  (x <mark>==</mark> h) || inventoryContains x t
-- No instance for (Eq a) arising from a use of ‘==’
</code></pre>

---

###### Suppose the following scenario

* What we really want to say is
* Find it in the inventory *for anything that supports comparison for equality*

<pre><code class="language-haskell hljs" data-trim data-noescape>
inventoryContains :: <mark>Eq a</mark> => a -> [a] -> Bool
inventoryContains _ [] =
  False
inventoryContains x (h:t) =
  (x == h) || inventoryContains x t
</code></pre>

---

###### Suppose the following scenario

* We utilise a type class constraint on the function type signature
* Notice that this function will work with an `Automobile`
* Even though `Automobile` does not appear at all in our type signature

<pre><code class="language-haskell hljs" data-trim data-noescape>
inventoryContains :: Eq a => a -> [a] -> Bool
inventoryContains _ [] =
  False
inventoryContains x (h:t) =
  (x == h) || inventoryContains x t
</code></pre>

---

###### Suppose the following scenario

* We have utilised a software engineering principle here
* We have made our code more *reusable*
* This was achieved without any extra effort

<pre><code class="language-haskell hljs" data-trim data-noescape>
inventoryContains :: Eq a => a -> [a] -> Bool
inventoryContains _ [] =
  False
inventoryContains x (h:t) =
  (x == h) || inventoryContains x t
</code></pre>

---

###### This principle and Functional Programming

* It is a common Functional Programming practice to extend this principle **much further**
* For no other reason than *we can*
* Let's not forget the fundamental definition of Functional Programming

---

## A less trivial example

###### Suppose these functions already exist and have been written

<pre><code class="language-haskell hljs" data-trim data-noescape>
mapList ::     (a -> b) -> List     a -> List     b
mapOptional :: (a -> b) -> Optional a -> Optional b
mapParser ::   (a -> b) -> Parser   a -> Parser   b
mapVector6 ::  (a -> b) -> Vector6  a -> Vector6  b
</code></pre>

---

## A less trivial example

###### We then write this function for <code>List</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipList :: List (a -> b) -> a -> List b
flipList list a =
  mapList (\func -> func a) list
</code></pre>

---

## A less trivial example

###### and then for <code>Optional</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipOptional :: Optional (a -> b) -> a -> Optional b
flipOptional opt a =
  mapOptional (\func -> func a) opt
</code></pre>

---

## A less trivial example

###### and then for <code>Parser</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipParser :: Parser (a -> b) -> a -> Parser b
flipParser prsr a =
  mapParser (\func -> func a) prsr
</code></pre>

---

## A less trivial example

###### and then for <code>Vector6</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipVector6 :: Vector6 (a -> b) -> a -> Vector6 b
flipVector6 v6 a =
  mapVector6 (\func -> func a) v6
</code></pre>

---

## A less trivial example

###### What might we consider doing? What would that code look like?

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipList ::
  List (a -> b) -> a -> List b
flipList list a =
  mapList (\func -> func a) list

flipOptional ::
  Optional (a -> b) -> a -> Optional b
flipOptional opt a =
  mapOptional (\func -> func a) opt

flipParser ::
  Parser (a -> b) -> a -> Parser b
flipParser prsr a =
  mapParser (\func -> func a) prsr

flipVector6 ::
  Vector6 (a -> b) -> a -> Vector6 b
flipVector6 v6 a =
  mapVector6 (\func -> func a) v6
</code></pre>

---

## A less trivial example

###### Refactor out the differences

<pre><code class="language-haskell hljs" data-trim data-noescape>
mapList ::     (a -> b) -> List     a -> List     b
mapOptional :: (a -> b) -> Optional a -> Optional b
mapParser ::   (a -> b) -> Parser   a -> Parser   b
mapVector6 ::  (a -> b) -> Vector6  a -> Vector6  b
</code></pre>

---

## A less trivial example

###### Remember this game?

<div style="text-align:center">
  <a href="images/spot-the-difference.png">
    <img src="images/spot-the-difference.png" alt="Spot the difference" style="width:640px"/>
  </a>
</div>

---

## A less trivial example

###### The differences

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipList ::
  <mark>List</mark> (a -> b) -> a -> <mark>List</mark> b
flipList list a =
  <mark>mapList</mark> (\func -> func a) list

flipOptional ::
  <mark>Optional</mark> (a -> b) -> a -> <mark>Optional</mark> b
flipOptional opt a =
  <mark>mapOptional</mark> (\func -> func a) opt

flipParser ::
  <mark>Parser</mark> (a -> b) -> a -> <mark>Parser</mark> b
flipParser prsr a =
  <mark>mapParser</mark> (\func -> func a) prsr

flipVector6 ::
  <mark>Vector6</mark> (a -> b) -> a -> <mark>Vector6</mark> b
flipVector6 v6 a =
  <mark>mapVector6</mark> (\func -> func a) v6
</code></pre>

---

## A less trivial example

###### The differences

<pre><code class="language-haskell hljs" data-trim data-noescape>
mapList ::     (a -> b) -> <mark>List</mark>     a -> <mark>List</mark>     b
mapOptional :: (a -> b) -> <mark>Optional</mark> a -> <mark>Optional</mark> b
mapParser ::   (a -> b) -> <mark>Parser</mark>   a -> <mark>Parser</mark>   b
mapVector6 ::  (a -> b) -> <mark>Vector6</mark>  a -> <mark>Vector6</mark>  b
</code></pre>

---

## A less trivial example

###### We want to express an abstraction, "anything that has a `map` function"

* We will define a type class
* <div style="color: gray">Next we will `instance` that type class for data types</div>
* <div style="color: gray">Finally, we write our use-case function *only once*</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
class CanMap k where
  mapAnything :: (a -> b) -> k a -> k b
</code></pre>

---

## A less trivial example

* <div style="color: gray">We will define a type class</div>
* Next we will `instance` that type class for data types
* <div style="color: gray">Finally, we write our use-case function *only once*</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance CanMap List where &hellip;
instance CanMap Optional where &hellip;
instance CanMap Parser where &hellip;
instance CanMap Vector6 where &hellip;
</code></pre>

---

## A less trivial example

* <div style="color: gray">We will define a type class</div>
* <div style="color: gray">Next we will `instance` that type class for data types</div>
* Finally, we write our use-case function *only once*

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipAnything :: CanMap k => k (a -> b) -> a -> k b
flipAnything x a =
  mapAnything (\func -> func a) x
</code></pre>

---

## A less trivial example

###### Some observations

* We have followed the same software engineering principle as described earlier
* This refactoring is only possible _because we are functional programming_
* There is a *specific shape* to the things that fit as an instance of this type class
  * Consider:
  * `instance CanMap Int where`
  * we now must write a function `:: (a -> b) -> Int a -> Int b`
  * What is a value with type `Int a`? This makes no sense!
  * We have a *type of type* error

---

## A less trivial example

###### Some observations

* Candidate data type instances for `instance CanMap` must be *any type that takes a type to a type*
* `Int` is not such a candidate
* `List` is a candidate
* In other words, *a type can have a type*
* The type of types is often called its *kind*
* GHCi will report the type of a type using `:kind` (or `:k`)

---

## Experiment

###### Experiment with GHCi

* `> :kind Int`
* `> :kind List`
* `> :kind Vector6`
* `> :kind Optional`
* `> :kind (->)`
* `> :kind List Int`
* `> :kind Optional (List Int)`
* `> :kind (->) Int`
* `> :kind (->) Int String`

---

## Nomenclature

* The type of types is disambiguated by the type of regular values by "kind"
* `List` is itself a function, that takes a type as an argument, and returns a type
* For example, `List Int` is `List` applied to `Int` to return a list of int (a type)
* "the kind of `List` is type to type"

---

## Nomenclature

###### Remember: **all functions take one argument**

* `data Pair a b = Pair a b`
* `> :kind Pair`
* `> :kind Pair Int`

---

## More nomenclature!

###### CanMap better known as &hellip;

* `class CanMap k where mapAnything :: (a -> b) -> k a -> k b`
* This *particular* abstraction has an established name since around the 1940s
* Therefore, we will use it

---

## More nomenclature!

###### CanMap better known as &hellip;

* `class Functor k where fmap :: (a -> b) -> k a -> k b`
* It is known as a *covariant functor* or sometimes just "functor"
* Its operation is often pronounced "effmap"

---

## Functor

* Since class `Functor` is well-established and is in the Haskell base library (`:info Functor`) &hellip;
* Explore some of its potential instances:

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
instance Functor List where -- todo
data Pair a b = Pair a b
instance Functor (Pair a) where -- todo
data Tree a = Node a [Tree a]
instance Functor Tree where -- todo
data Function a b = Function (a -> b)
instance Functor (Function a) where -- todo
</code></pre>

---

## Functor

* What about this?
* Why or why not can you write `instance Functor`
* Try it!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data FlippedFunction a b = FlippedFunction (b -> a)
instance Functor (FlippedFunction a) where -- todo
</code></pre>

---

## Superclass and Subclass Design

* We might say, for example, "all data types with `instance Ord` also have `instance Eq`"
* For example, `Int` has `instance Ord` and therefore it follows, `Int` has `instance Eq`
* However, complex numbers have `instance Eq` but **not** `instance Ord`
* *the set of instances of `Ord` is a **strict subset** of the set of instances of `Eq`*
* `Eq` is a superclass of `Ord` &hellip; `class Eq a => Ord a where`

---

## Superclass and Subclass Design

* There is a set of *derived operations* with the constraint `Ord`
* For example, sorting a list, or finding a maximum element of a list
* There is also a set of *derived operations* with the constraint `Eq`
* Due to the superclass/subclass relationship &hellip;
* *the set of derived operations with a superclass constraint is a **strict superset** of those with the subclass constraint*
* Ideally, in program design, this holds true for all superclass/subclass relationships

---

## Superclass and Subclass Design

<div style="text-align:center">
  <a href="images/superclass-subclass-labelled-01.png">
    <img src="images/superclass-subclass-labelled-01.png" alt="Spot the difference" style="width:920px"/>
  </a>
</div>


---

## Superclass and Subclass Design

<div style="text-align:center">
  <a href="images/superclass-subclass-examples-02.png">
    <img src="images/superclass-subclass-examples-02.png" alt="Spot the difference" style="width:920px"/>
  </a>
</div>

