## A bit more Haskell syntax { data-transition="fade none" }

###### Modules

* Haskell modules are declared using the `module` and `where` keywords
* The declaration appears (at most) once per file, and at the top of the file
* The module name starts with a capital letter and corresponds to the file name
* modules can then be imported using the `import` keyword

---

## A bit more Haskell syntax { data-transition="fade none" }

###### For example

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- filename: MyProject/MyModule.hs
module MyProject.MyModule where
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- filename: MyProject/MyOtherModule.hs
module MyProject.MyOtherModule where

import MyProject.MyModule
-- code that uses declarations in MyModule
</code></pre>

---

## A bit more Haskell syntax { data-transition="fade none" }

###### Modules

* It is generally a good idea to have one data type or type class per module (file)
* Also in that module are either:
  * Functions that relate to that data type
  * Instances of that type class
* Sometimes it is necessary to break this rule
* *This is a general guideline for structuring and organising code into modules*

---

## A bit more Haskell syntax { data-transition="fade none" }

###### For example

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- filename: DataType/List.hs
module DataType.List where

data List a = Nil | Cons a (List a)

headOr :: a -> List a -> a
headOr x Nil = x
headOr _ (Cons a _) = a

...
</code></pre>

---

## A bit more Haskell syntax { data-transition="fade none" }

###### For example

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- filename: TypeClass/ToInt.hs
module TypeClass.ToInt where

class ToInt a where
  toInt :: a -> Int

instance ToInt Int where
  toInt x = x
...
</code></pre>

---

## A bit more Haskell syntax { data-transition="fade none" }

###### let/in

* Sometimes a function uses an expression more than once
* We wish to name that expression
* However, we wish for that name to be *local to the function only*
* We would use the `let` and `in` keywords

---

## A bit more Haskell syntax { data-transition="fade none" }

###### For example

* The expression `x + x` repeats
* We wish to name that expression and use the name instead

<pre><code class="language-haskell hljs" data-trim data-noescape>
func x =
  (x + x) * (x + x)
</code></pre>

---

###### For example

* Let the name `y` be equal to the expression `x + x`
* The name `y` is only available within the `func` function

<pre><code class="language-haskell hljs" data-trim data-noescape>
func x =
  let y = x + x
  in  y * y
</code></pre>

---

## A bit more Haskell syntax { data-transition="fade none" }

###### Newtype

* When declaring data types, we can replace `data` with `newtype` **only if**
  * Our data type **has one constructor**
  * That constructor **has one argument**
* At compile-time, the constructor will be removed for performance reasons
* *However, the type will remain and will be checked by the compiler*

---

## A bit more Haskell syntax { data-transition="fade none" }

###### For example

<pre><code class="language-haskell hljs" data-trim data-noescape>
newtype MyString = MyString String

-- A completely new type that does not unify with String
-- String and MyString are separate types
-- This is useful to provide different type class instances
-- Remember: maximum one type class instance per data type
</code></pre>

---

## A bit more Haskell syntax { data-transition="fade none" }

###### Record selector syntax

* Data type constructors can have names for their arguments
* This creates "get" and "set" functions for those arguments

---

## A bit more Haskell syntax { data-transition="fade none" }

###### For example

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- data Person = Person Int String

-- with record selector syntax
data Person =
  Person {
    age :: Int
  , name :: String
  }
</code></pre>

---

## A bit more Haskell syntax { data-transition="fade none" }

###### For example

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Person =
  Person {
    age :: Int
  , name :: String
  }
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
> :type age
age :: Person -> Int
> :type name
name :: Person -> String
</code></pre>

---

## A bit more Haskell syntax { data-transition="fade none" }

###### We can also "set" with record selector syntax

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Person =
  Person {
    age :: Int
  , name :: String
  }

birthday :: Person -> Person
birthday p = p { age = age p + 1 }
</code></pre>

